package UseComparatorandComparable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;


public class ComparableandSortTester {
	public static void main(String args[]){
		
		System.out.println("<<PERSON>>");
		System.out.println();
		ArrayList<Person> personer = new ArrayList<Person>();
		personer.add(new Person("David",1200000));
		personer.add(new Person("Lily",1000000));
		personer.add(new Person("Mike",580000));
		
		System.out.println("--before sort array in Person--");
		for(Person p : personer)
			System.out.println(p);
		
		Collections.sort(personer);
		
		System.out.println("\n--after sort array in Person--");
		
		for(Person p : personer)
			System.out.println(p);
		
		System.out.println();
		System.out.println("<<PRODUCT>>");
		System.out.println();
		ArrayList<Product> product = new ArrayList<Product>();
		product.add(new Product("Macbook",45000));
		product.add(new Product("Ipad",22000));
		product.add(new Product("Shoes",500));
		
		System.out.println("--before sort array in Product--");
		for(Product p : product)
			System.out.println(p);
		
		Collections.sort(product);
		
		System.out.println("\n--after sort array in Product--");
		
		for(Product p : product)
			System.out.println(p);
	}
	

}
