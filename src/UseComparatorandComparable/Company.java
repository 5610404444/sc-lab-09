package UseComparatorandComparable;
import java.util.Comparator;





public class Company implements Taxable{
	
	private String nameCompany ;
	private double income;
	private double outcome;
	private double profit;


	public Company(String name, double in, double out) {
		this.nameCompany = name;
		this.income = in;
		this.outcome = out;
		this.profit = this.income-this.outcome;
		
	}


	@Override
	public double getTax() {
		
		return (income-outcome)*(0.3);
	}
	
	public String getNameCompany(){
		return nameCompany;
	}
	
	public double getIncome(){
		return income;
	}
	
	public double getOutcome(){
		return outcome;
	}
	
	public double getProfit(){
		return profit;
	}
	
	public String toString() {
		return "Company< name = "+this.nameCompany+", income = "+this.income+", outcome = "+this.outcome +">";
	}


	


}
