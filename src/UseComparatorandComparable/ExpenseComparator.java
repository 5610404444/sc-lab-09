package UseComparatorandComparable;
import java.util.Comparator;



public class ExpenseComparator implements Comparator{

	@Override
	public int compare(Object o1, Object o2) {
		double out1 = ((Company)o1).getOutcome();
		double out2 = ((Company)o2).getOutcome();
		if (out1 > out2) return 1;
		if (out1 < out2) return -1;
		return 0;
	}
	

}
