package UseComparatorandComparable;
import java.util.Comparator;



public class Product implements Taxable,Comparable<Product> {
	private String nameProduct;
	private double price;

	public Product(String name, double price) {
		this.nameProduct = name;
		this.price = price;
	}

	@Override
	public double getTax() {
		
		return (price*7)/100;
	}
	public String toString() {
		return "Product <name = "+this.nameProduct+", Price = "+this.price+">";
	}


	@Override
	public int compareTo(Product other) {
		
		if(this.price<other.price){
			return -1;
		}
		if(this.price>other.price){
			return 1;
		}
		return 0;
	}

	
	
}
