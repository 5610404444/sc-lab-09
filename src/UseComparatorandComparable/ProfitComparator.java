package UseComparatorandComparable;


import java.util.Comparator;

public class ProfitComparator implements Comparator{

	@Override
	public int compare(Object p1, Object p2) {
		double profit1 = ((Company)p1).getProfit();
		double profit2 = ((Company)p2).getProfit();
		if (profit1 > profit2) return 1;
		if (profit1 < profit2) return -1;
		return 0;
		
	}

}
