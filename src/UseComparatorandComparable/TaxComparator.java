package UseComparatorandComparable;
import java.util.Comparator;


public class TaxComparator implements Comparator{

	@Override
	public int compare(Object t1, Object t2) {
		double tax1 = ((Taxable)t1).getTax();
		double tax2 = ((Taxable)t2).getTax();
		if (tax1 > tax2) return 1;
		if (tax1 < tax2) return -1;
		return 0;
	}

}
