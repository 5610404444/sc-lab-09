package UseComparatorandComparable;
import java.util.ArrayList;
import java.util.Collections;


public class ComparatorTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("<<COMPANY>>");
		System.out.println();
		ArrayList<Company> companies = new ArrayList<Company>();
		companies.add(new Company("Starbucks",5000000,1800000));
		companies.add(new Company("Walls",500000,200000));
		companies.add(new Company("B2S",1200000,900000));
		
		
		System.out.println("--Before sort array--");
		
		for (Company c : companies){
			System.out.println(c);
		}
		
		Collections.sort(companies,new EarningComparator());
		System.out.println("\n---After sort with Earning(income) ");
		for (Company c : companies){
			System.out.println(c);
		}
		
		Collections.sort(companies,new ExpenseComparator());
		System.out.println("\n---After sort with Expense(outcome) ");
		for (Company c : companies){
			System.out.println(c);
		}
		

		Collections.sort(companies,new ProfitComparator());
		System.out.println("\n---After sort with Profit ");
		for (Company c : companies){
			System.out.println(c);
		}
		
		
		
		
		
	
	 }
	


}
