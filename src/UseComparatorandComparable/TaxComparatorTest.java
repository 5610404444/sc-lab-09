package UseComparatorandComparable;
import java.util.ArrayList;
import java.util.Collections;


public class TaxComparatorTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Taxable> mix_data = new ArrayList<Taxable>();
		Person person = new Person("Joe",100000);
		Company company= new Company("Univesal",1000000,800000);
		Product product = new Product("Powerbank",500);
		mix_data.add(person);
		mix_data.add(company);
		mix_data.add(product);
		
		System.out.println("--Before sort array--");
		
		for (Taxable t : mix_data){
			System.out.println(t);
		}
		
		Collections.sort(mix_data,new TaxComparator());
			
		
		System.out.println("\n---After sort with Tax-- ");
		for (Taxable t : mix_data){
			System.out.println(t);
		}
		

	}

}
