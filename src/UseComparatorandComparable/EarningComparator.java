package UseComparatorandComparable;
import java.util.Comparator;



public class EarningComparator implements Comparator{

	@Override
	public int compare(Object e1, Object e2) {
		double earn1 = ((Company)e1).getIncome();
		double earn2 = ((Company)e2).getIncome();
		if (earn1 > earn2) return 1;
		if (earn1 < earn2) return -1;
		return 0;
	}

}
