package TreeTraversal;

import java.util.ArrayList;

public class ReportConsole {
	
	public  void display(Node root,Traversal traversal){
		
		
		ArrayList<Node> printNode = traversal.traverse(root);
		
		for(int p = 0; p < printNode.size();p++){
			
			System.out.print(printNode.get(p)+" ");
			
		}
		
		
	}

}
