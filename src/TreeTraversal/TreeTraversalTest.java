package TreeTraversal;

public class TreeTraversalTest {
	
	public static void main(String args[]){
		
		ReportConsole  report = new ReportConsole();
		
		Node c = new Node("C",null,null);
		Node e = new Node("E",null,null);
		Node d = new Node("D",c,e);
		Node a = new Node("A",null,null);
		Node b = new Node("B",a,d);
		Node h = new Node("H",null,null);
		Node i = new Node("I",h,null);
		Node g = new Node("G",null,i);
		Node f = new Node("F",b,g);
		
		System.out.println("--Tree Traverse Test--");
		System.out.print("Traverse with PreOrderTraversal : ");
		report.display(f,new PreOrderTraversal());
		System.out.println();
		
		System.out.print("Traverse with InOrderTraversal : ");
		report.display(f,new InOrderTraversal());
		System.out.println();
		
		System.out.print("Traverse with PostOrderTraversal : ");
		report.display(f,new PostOrderTraversal());
		
		
	}

}
