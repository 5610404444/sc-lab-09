package TreeTraversal;

import java.util.ArrayList;


public class PreOrderTraversal implements Traversal {

	@Override
	public ArrayList<Node> traverse(Node node) {
		
		ArrayList<Node> nodeResult = new ArrayList<Node>();
		
		nodeResult.add(node);
		
		if(node.getLeft() != null){
			
			ArrayList<Node> nodeArraylst = traverse(node.getLeft());
			
			for (int i = 0; i < nodeArraylst.size();i++){
				nodeResult.add(nodeArraylst.get(i));
			}
		}
		
		if(node.getRight() != null){
			
			ArrayList<Node> nodeArraylst = traverse(node.getRight());
			
			for (int i = 0; i < nodeArraylst.size();i++){
				nodeResult.add(nodeArraylst.get(i));
			}
		}
		
		return nodeResult;
	}

}
