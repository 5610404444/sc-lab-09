package TreeTraversal;

import java.util.ArrayList;


public class PostOrderTraversal implements Traversal{

	@Override
	public ArrayList<Node> traverse(Node  node) {
		ArrayList<Node> nodeResult = new ArrayList<Node>();
		
		if(node.getLeft() != null){
			ArrayList<Node> nodeArraylst = traverse(node.getLeft());
			
			for (int i = 0;i < nodeArraylst.size();i++){
				nodeResult.add(nodeArraylst.get(i));
			}
		}
			
		if(node.getRight() != null){
			ArrayList<Node> nodeArraylst = traverse(node.getRight());
				
			for(int i = 0;i < nodeArraylst.size();i++){
				nodeResult.add(nodeArraylst.get(i));
			}
		}
		
		nodeResult.add(node);
		
		return nodeResult;
	}

}
