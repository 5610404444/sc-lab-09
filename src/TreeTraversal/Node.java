package TreeTraversal;

public class Node {
	private String value;
	private Node left;
	private Node right;
	
	public Node(String aValue, Node aLeft, Node aRight) {
		value = aValue;
		left = aLeft;
		right = aRight;
	}

	public String getValue(){
		return value;
	}
	
	public Node getLeft(){
		return left;
	}
	
	public Node getRight(){
		return right;
	}
	
	public String toString(){
		return getValue();
	}

}
